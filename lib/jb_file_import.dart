library jb_file_import;

import 'package:angular/angular.dart';
import 'package:jb_bool_attr/jb_bool_attr.dart';
import 'src/component.dart';

// Export any libraries here which are intended for clients of this package.

export 'src/component.dart';


class JbFileImportModule extends Module {

  JbFileImportModule () {
    bind(JbBoolAttr);
    bind(JbFileImport);
  }
}