library jb_file_import.component;

import 'dart:html';

import 'package:angular/angular.dart';
import 'package:logging/logging.dart';
import 'package:verbal_expressions/verbal_expressions.dart';

@Component(
    selector: "jb-file-import[files]",
    templateUrl: "packages/jb_file_import/src/component_template.html",
    useShadowDom: true)
class JbFileImport implements ShadowRootAware, AttachAware {
  static const String classDropHint = "drop-hint";
  static const String classNoImagePlaceholder = "no-image-placeholder";
  static const String defaultMessage1 = "Datei hierher ziehen und loslasen oder hier klicken zum Auswählen";
  static const String defaultMessage2 = "Datei(en) hierher ziehen und loslasen oder hier klicken zum Auswählen";
  static const String dropHintMessage1 = "Datei hier ablegen";
  static const String dropHintMessage2 = "Datei(en) hier ablegen";
  Logger _logger = new Logger("jb_file_import.component");

  bool _attributesLoaded = false;
  bool _templateLoaded = false;
  Element _node;

  DivElement _dropContainer;
  InputElement _fileSelect;

  @NgOneWayOneTime('width')
  String width = "300px";

  @NgOneWayOneTime('height')
  String height = "200px";

  @NgOneWayOneTime('color')
  String defaultColor = "rgba(100, 100, 100, 0.5)";

  @NgOneWayOneTime('color-hover')
  String hoverColor = "#373737";

  @NgTwoWay("files")
  List<File> files;

  ///mine-types or suffixes
  @NgOneWay("allowed-types")
  List<String> allowedTypeStrings;

  List<VerbalExpression> allowedTypeExpressions = [];

  bool allowMultipleFiles = false;
  String currentMessage = defaultMessage1;

  List<String> messageClasses = [classNoImagePlaceholder];

  JbFileImport(this._node) {}

  @override
  void attach() {
    if (_node.attributes.containsKey("allow-multiple-files") || _node.attributes.containsKey("allow-multifile"))
      allowMultipleFiles = true;

    //compile allowedTypeStrings to allowedTypeExpressions
    allowedTypeStrings.forEach((typeString) {
      var typeExpression = new VerbalExpression()
        ..startOfLine();

      if (typeString.contains("/")) {
        var temp = typeString.split("/");
        var category = temp[0];
        var detailedType = temp[1];

        typeExpression..then(category)..then("/");

        switch (detailedType) {
          case "*":
            typeExpression..something();
            break;
          default:
            typeExpression.then(detailedType);
            break;
        }
      } else if (typeString.contains(".")) {
        typeExpression
          ..then(".")
          ..something();
      }

      typeExpression.endOfLine();
      allowedTypeExpressions.add(typeExpression);
    });

    _attributesLoaded = true;
    onLoadComplete();
  }

  disableDropHint(Event e) {
    e.preventDefault();
    currentMessage = (allowMultipleFiles) ? defaultMessage2 : defaultMessage1;
  }

  enableDropHint(Event e) {
    e.preventDefault();
    currentMessage = (allowMultipleFiles) ? dropHintMessage2 : dropHintMessage1;
  }

  ///Main file import handler
  onDropHandler(MouseEvent e) {
    //remove drop-hint classes
    disableDropHint(new Event("MouseEvent"));
    setDefaultStyles();

    List<File> droppedFiles = e.dataTransfer.files;
    _logger.finest("files.length: ${droppedFiles.length}");

    if (droppedFiles.length > 1) {
      if (!allowMultipleFiles) {
        _logger.warning("More than one file inserted with option 'allow-multiple-images' disabled. "
            "Only the first file will be processed");
        var file = droppedFiles[0];

        files = filterFiletypes([file]);
        return;
      }

      files = filterFiletypes(droppedFiles);
    } else if (droppedFiles.length < 1) {
      _logger.severe("No local files droped");
    } else {
      files = filterFiletypes([droppedFiles[0]]);
    }
  }

  onFileSelect() => files = _fileSelect.files;

  bool isDroppedFiletypeAllowed(String type) =>
      allowedTypeExpressions.any((typeExpression) => typeExpression.hasMatch(type));

  List<File> filterFiletypes(List<File> files) {
    List<File> result = [];

    files.forEach((file) {
      if (isDroppedFiletypeAllowed(file.type)) {
        result.add(file);
      } else {
        _logger.severe("The Filetype ${file.type} of the file ${file.name} is not allowed.");
      }
    });

    return result;
  }


  void onLoadComplete() {
    if (!(_attributesLoaded && _templateLoaded)) {
      return;
    }

    _dropContainer.style
      ..width = width
      ..height = height;

    setDefaultStyles();

    //document - register event listeners
    document
      ..onDragEnter.listen(enableDropHint)
      ..onDragOver.listen(enableDropHint)
      ..onDrop.listen(disableDropHint)
      ..onDragLeave.listen((MouseEvent e) {
        e.preventDefault();
        Element target = e.target as Element;
        if (target != null && !_node.contains(target)) disableDropHint(new Event("MouseEvent"));
      });

    //_dragHint - register event listeners
    _dropContainer
      ..onDragEnter.listen((MouseEvent e) => setHoverStyles())
      ..onDragOver.listen((MouseEvent e) => setHoverStyles())
      ..onDragLeave.listen((MouseEvent e) => setDefaultStyles())
      ..onDrop.listen(onDropHandler);
  }

  @override
  void onShadowRoot(ShadowRoot shadowRoot) {
    _dropContainer = shadowRoot.querySelector(".dropContainer");
    _fileSelect = shadowRoot.querySelector("#fileSelect");

    _templateLoaded = true;
    onLoadComplete();
  }

  void openFileDialog() {
    _fileSelect.click();
  }

  void setDefaultStyles() {
    _dropContainer.style
      ..borderColor = defaultColor
      ..color = defaultColor;
  }

  void setHoverStyles() {
    _dropContainer.style
      ..borderColor = hoverColor
      ..color = hoverColor;
  }
}
