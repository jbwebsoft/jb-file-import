# Changelog

## 1.2.0
- fixed allowed-types restriction implementation

## 1.1.0
- added file import per file picker 

## 1.0.2 
- make width and height accept default value with making them NgOneWayOneTime

## 1.0.1 
- changed width and height attrs on component to be css dimension strings (e.g. 200px or 100%)

## 1.0.0
- Initial version
