# jb_file_import

A drag and drop file import component for angular dart 1

## Installation
1. Add the component to your pubspec.yaml

    - Use git https version (better for public dependencies)

            dependencies:
              jb_file_import:
                git:
                  url: https://<URL einfügen>

    - Git with specific branch

            dependencies:
              jb_file_import:
                git:
                  url: https://<URL einfügen>
                  ref: some-branch | tag

2. Show your component to angular dependency injection -> see App Class in app_initialize.dart

        import 'package:jb_file_import/jb_file_import.dart';

        class App extends Module {
          App() {
            bind(JbFileImport);
          }
        }

## Tag Usage

    <jb-file-import> </jb-file-import>

### Attributes

- **width**
    - Type: css dimension string (e.g. 100% or 200px)
    
- **height**
    - Type: css dimension string (e.g. 100% or 200px)
    
- **color**
    - Sets border and font color in default state
    - Type: css color string
    
- **color-hover**
    - Sets border and font color when you drag over the component
    - Type: css color string

- **files**
    - Type: two way bound List<File> 

- **allow-multifile** or **allow-multiple-files** 
    - Type: boolean attribute [optional]
    - Allow multiple file import with attributes 



## Some further knowledge hints

- Obtaining injector:

        final injector = applicationFactory().addModule(new SpeedpadApp()).run();

- Logger Configuration:

        //init logging
        hierarchicalLoggingEnabled = true;
        Logger.root.onRecord.listen(new LogPrintHandler());

        Logger.root.level = Level.OFF;
        _libLogger.level = Level.ALL;
        //could be customized with _libLogger.level =  Level.INFO or Level.OFF and
        // then add specific logger
        //_logger.level = Level.All

- DI register possibilities:

        //EXAMPLE: bind(StorageService);
        //EXAMPLE: bind(StorageService, toValue: new StorageService());
        //EXAMPLE: bind(StorageService, toFactory: (Angular.Injector inj) => new StorageService(inj.get(EventBus)));